using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Transform Target;
    private float CurrentDist = 10, TranslateSpeed = 100, AngleH, AngleV;
    public float timeSinceAction = 0.0f;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        AngleH = -90;
        AngleV = 80;
    }        
    // Update is called once per frame
     public void Update()
     {
        timeSinceAction += Time.deltaTime;
        AngleH += Input.GetAxis("Mouse X");
        AngleV -= Input.GetAxis("Mouse Y");
        CurrentDist += Input.GetAxis("Mouse ScrollWheel");
        if(AngleV < 5){
            AngleV = 5;
        }
        if(AngleV > 80){
            AngleV = 80;
        }
     }
    public void LateUpdate()
    {
        Vector3 tmp;
        tmp.x = Mathf.Cos(AngleH * (Mathf.PI / 180)) * Mathf.Sin(AngleV * (Mathf.PI / 180)) * CurrentDist + Target.position.x;
        tmp.z = Mathf.Sin(AngleH * (Mathf.PI / 180)) * Mathf.Sin(AngleV * (Mathf.PI / 180)) * CurrentDist + Target.position.z;
        tmp.y = CurrentDist + Target.position.y;
        transform.position = Vector3.Slerp(transform.position, tmp, TranslateSpeed * Time.deltaTime);
        transform.LookAt(Target);
    }
}
