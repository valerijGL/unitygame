using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    static bool canJump = true;
    public float JumpForce = 150f;
    public Rigidbody _rb;
    private bool _isGrounded;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKey("w")){
            _rb.AddForce(0, 0, 4000 * Time.deltaTime);
        }
        if(Input.GetKey("s")){
            _rb.AddForce(0, 0, -5500 * Time.deltaTime);
        }
        if(Input.GetKey("d"))
        {
            _rb.AddForce(100, 0, 0 * Time.deltaTime);
        }
        if(Input.GetKey("a"))
        {
            _rb.AddForce(-100, 0, 0 * Time.deltaTime);
        }
        if (Input.GetAxis("Jump") > 0)
        {
            if (_isGrounded)
            {
                _rb.AddForce(Vector3.up * JumpForce);
            }
        }
        JumpLogic();
    }
    private void JumpLogic()
    {
        if (Input.GetAxis("Jump") > 0)
        {
            if (_isGrounded)
            {
                _rb.AddForce(Vector3.up * JumpForce);
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        IsGroundedUpate(collision, true);
    }

    void OnCollisionExit(Collision collision)
    {
        IsGroundedUpate(collision, false);
    }

    private void IsGroundedUpate(Collision collision, bool value)
    {
        if (collision.gameObject.tag == ("Ground"))
        {
            _isGrounded = value;
        }
    }
}
