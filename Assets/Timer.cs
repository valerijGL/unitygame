using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;


public class Timer : MonoBehaviour
{
    public Image time;
    public Text timeAmount;
    double scoreValueRound;
    public static float scoreValue = 0;
    public float maTime = 20f;
    float timeLeft;
    void Start()
    {
        timeLeft = maTime;
        scoreValue = timeLeft;
    }

    void Update()
    {
        if (timeLeft > 0)
        {
            scoreValue = scoreValue - 1 * Time.deltaTime;
            scoreValueRound = Math.Round(scoreValue, 0);
            timeAmount.text = " " + scoreValueRound;
            timeLeft -= Time.deltaTime;
            time.fillAmount = timeLeft / maTime;

        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }
}
